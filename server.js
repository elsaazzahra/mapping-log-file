const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();

app.use(fileUpload());

// Upload Endpoint
app.post('/upload', (req, res) => {
  if (req.files === null) {
    return res.status(400).json({ msg: 'No file uploaded' });
  }

  const file = req.files.file;

  file.mv(`${__dirname}/client/public/uploads/${file.name}`, err => {
    if (err) {
      console.error(err);
      return res.status(500).send(err);
    }

    res.json({ fileName: file.name, filePath: `/uploads/${file.name}` });
  });
});

var path = require('path'), fs = require('fs');

function fromDir(callback){
  //console.log('Starting from dir '+startPath+'/');
  if (!fs.existsSync('./client/public/uploads')){
      console.log("no dir ",'./client/public/uploads');
      return;
  }

  var files=fs.readdirSync('./client/public/uploads');
  for(var i=0;i<files.length;i++){
      var filename=path.join('./client/public/uploads',files[i]);
      var stat = fs.lstatSync(filename);
      if (stat.isDirectory()){
          fromDir(filename,/\.nmf$/,callback); //recurse
      }
      else if (/\.nmf$/.test(filename)) callback(filename);
  };
};

app.get('/var', (req, res) => {
  fromDir(function(filename){
    var str = filename;
    var rees = str.substr(22);
    // console.log(rees);
    return rees;
  });
})

app.get('/run', (req, res) => {

  const { spawn } = require('child_process');
  const pyProg = spawn('python', ['./api/python/parsing_dt/parsing_cellmeas.py','/home/immobi/PROJECT/Project_React/mapping-log-file/client/public/uploads/']);

  pyProg.stdout.on('data', function(data) {

      console.log(data.toString());
      res.write(data);
      // res.end(data);
  });
})

app.listen(5000, () => console.log('Server Started...'));
