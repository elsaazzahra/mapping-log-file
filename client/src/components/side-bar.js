import React, {useState} from 'react';
import { slide as Menu } from 'react-burger-menu';
import { Button, Modal } from 'react-bootstrap';
import FileUpload from './FileUpload';

export default props => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <div class="row">
      <div class="col text-center">
        <Button variant="primary" onClick={handleShow}>
          Import Logfile
        </Button>
        <hr></hr>
      </div>

      <Modal show={show} onHide={handleClose} className='modalku'>
        <Modal.Header closeButton>
          <Modal.Title style={{color:"black"}}>Import Logfile</Modal.Title>
        </Modal.Header>
        <Modal.Body >
          <FileUpload/>
        </Modal.Body>
      </Modal>
    </div>
  );
};