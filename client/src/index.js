import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Side from './Side';
// import Filelist from './Filelist';

ReactDOM.render(<App />, document.getElementById('root'));
ReactDOM.render(<Side />, document.getElementById('side'));
// ReactDOM.render(<Filelist />, document.getElementById('filelist'));
