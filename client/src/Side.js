import React from 'react';
import Sidebar from './components/side-bar';
import './App.css';

class Side extends React.Component {
    constructor(){
        super()
        this.state = {
            // Set initial files, type 'local' means this is a file
            // that has already been uploaded to the server (see docs)
            files: [{
                source: 'index.html',
                options: {
                    type: 'local'
                }
            }]
        };
    }
  
    render(){
        return (
            <Sidebar/>
        )
    }
}
export default Side;
