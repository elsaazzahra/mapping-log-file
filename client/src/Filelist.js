import React from 'react';
import Filelist from './components/file-list';
import './App.css';

class Listfile extends React.Component {
    constructor(){
        super()
        this.state = {
            // Set initial files, type 'local' means this is a file
            // that has already been uploaded to the server (see docs)
            files: [{
                source: 'index.html',
                options: {
                    type: 'local'
                }
            }]
        };
    }
  
    render(){
        return (
            <Filelist/>
        )
    }
}
export default Listfile;
