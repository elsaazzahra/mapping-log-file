import React from 'react';
import FileUpload from './components/FileUpload';
import * as mapboxgl from 'mapbox-gl';
import Sidebar from './components/side-bar';
import './App.css';

class App extends React.Component {
  constructor(){
    super()
    this.mapRef = React.createRef()
    this.state = {
      // Set initial files, type 'local' means this is a file
      // that has already been uploaded to the server (see docs)
      files: [{
          source: 'index.html',
          options: {
              type: 'local'
          }
      }]
    };
  }

  componentDidMount(){
    mapboxgl.accessToken = 'pk.eyJ1IjoiZWxzYWF6emFocmEiLCJhIjoiY2sybjc0aHZhMDQ0bzNtb2MyNnFpMzE0ZyJ9.bJ_Cu-_iLWgbXRLmveLeSQ'; 
    
    // create mapbox object
    new mapboxgl.Map({
      container: this.mapRef.current,
      style:'mapbox://styles/mapbox/light-v10',
      center:[0,0],
      zoom: 1
    })
  }

  render(){
    return (
      <main class="app-content">
        <div style={{width: "100%", height:"100vh"}} ref={this.mapRef}></div>
      </main>
        //<Sidebar/>
    )
  }
}

export default App;
