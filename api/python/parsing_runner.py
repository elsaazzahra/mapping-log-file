import pandas as pd
from parsing_logfile.parsing_dt.parsing_cellmeas import load_nmf
from parsing_logfile.parsing_dt.parsing_cellmeas import parsing_cellmeas_2g
from parsing_logfile.parsing_dt.parsing_cellmeas import parsing_cellmeas_3g
from parsing_logfile.parsing_dt.parsing_cellmeas import parsing_cellmeas_4g

def parsing_runner(path):
    gps,month,cellmeas2g,cellmeas3g,cellmeas4g = load_nmf(path)

    parsing2g = parsing_cellmeas_2g(cellmeas2g, gps, month)
    parsing3g = parsing_cellmeas_3g(cellmeas3g, gps, month)
    parsing4g = parsing_cellmeas_4g(cellmeas4g, gps, month)

    return parsing2g, parsing3g, parsing4g

if __name__ == "__main__":
    pd.set_option('max_columns', 10000)
    pd.set_option('display.width', 10000)
    pd.set_option('display.max_rows', None)
    # pd.option_context('display.max_rows',  1000)
    path = '/home/immobi/Documents/logfile/OneDrive-2020-01-28/4G/'
    runn = parsing_runner(path)
    # df4g = parsing_cellmeas_4g(cellmeas4g,gps)

    # df4g.to_csv('D:/4g.csv',sep=',',encoding='iso-8859-1')
    print(runn)