import pandas as pd
import glob
import numpy as np
import csv
import os

def replace_column_nan(df, mean_data):
    df = df.replace(np.nan, mean_data)
    df = pd.DataFrame(df)
    return df

def load_nmf(path):
    all_files = glob.glob(path + "/*.nmf")
    li = []
    for filename in all_files:
        df = pd.read_fwf(filename)
        df = df.loc[:, ~df.columns.str.contains('^Unnamed')]
        df.columns=['column']
        k = pd.DataFrame(df.column.str.split(',').tolist())
        li.append(k)

        filename_first = len(filename)-4
        filename_second = len(path)
        filename_third = filename[filename_second:filename_first]
        filename_fourth = filename_third[:-2]
        path_export = path + filename_fourth
        # print(path_export)
        if not os.path.exists(path_export):
            os.mkdir(path_export)

    frame = pd.concat(li, axis=0, ignore_index=True, sort=False)
    gps = frame[frame[0].isin(['GPS'])]
    cellmeas = frame[frame[0].isin(['CELLMEAS'])]
    month = frame[frame[0].isin(['#START'])]

    month1 = month[[3]]
    month1.columns = ['month']
    month1['month'] = month1['month'].str[1:-1]
    a = month1['month']

    gps = gps[[0,1,3,4]]
    gps.columns = ['column','time','lon','lat']

    cellmeas4g = cellmeas[[0,1,3,4,5,6,7,8,9,10,11,12,13,14,15]]
    cellmeas4g.columns= ['column','time','tech','header_params','cells','params_cell','cell_type','band','ch','pci','rssi','rsrp','rsrq','timing','pathloss']
    cellmeas4g['scr_file'] = filename_fourth
    cell4g = cellmeas4g.loc[cellmeas4g.tech == '7']

    cellmeas3g = cellmeas[[0,1,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18]]
    cellmeas3g.columns= ['column','time','tech','header_params','chs','params_ch','ch1','rssi','band1','cells','params_cell','cell_type','band','ch','sc','ec','sttd','rscp']
    cellmeas3g['scr_file'] = filename_fourth
    cell3g = cellmeas3g.loc[cellmeas3g.tech == '5']

    cellmeas2g = cellmeas[[0,1,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22]]
    cellmeas2g.columns= ['column','time','tech','header_params','cells','params_cell','cell_type','band','arfcn','bsic','rxlev_full','rxlev_sub','c1','c2','c31','c32','hcs_priority','hcs_thr','cell_id','lac','rac','srxlev']
    cellmeas2g['scr_file'] = filename_fourth
    cell2g = cellmeas2g.loc[cellmeas2g.tech == '1']

    file_exportgps = path_export + "/" + "gps.csv"
    file_exportmonth = path_export + "/" + "month.csv"
    file_export4g = path_export + "/" + "4g.csv"
    file_export3g = path_export + "/" + "3g.csv"
    file_export2g = path_export + "/" + "2g.csv"

    gps.to_csv(file_exportgps,sep=',',encoding='iso-8859-1',header=True)
    a.to_csv(file_exportmonth,sep=',',encoding='iso-8859-1',header=True)
    cell4g.to_csv(file_export4g,sep=',',encoding='iso-8859-1',header=True)
    cell3g.to_csv(file_export3g,sep=',',encoding='iso-8859-1',header=True)
    cell2g.to_csv(file_export2g,sep=',',encoding='iso-8859-1',header=True)

    return file_exportgps, file_exportmonth, file_export2g, file_export3g, file_export4g

def parsing_cellmeas_4g(cellmeas, gps, month):
    # df1 = cellmeas
    # df2 = gps
    df1 = pd.read_csv(cellmeas,sep=',',encoding='iso-8859-1')
    df2 = pd.read_csv(gps,sep=',',encoding='iso-8859-1')
    df3 = pd.read_csv(month,sep=',',encoding='iso-8859-1')
    df3['month'] = df3['month'].str.replace('.', '/')
    # frame.sort_values("time", axis=0, ascending=True,inplace=True, na_position='last')
    # print(frame)
    b = []
    for i, d in df1.iterrows():
        if d['cell_type'] == 0:
            d['cell_type'] = "Serving"
            b.append(d)
        elif d['cell_type'] == 1:
            d['cell_type'] = "Listed"
            b.append(d)
        elif d['cell_type'] == 2:
            d['cell_type'] = "Detected"
            b.append(d)
        elif d['cell_type'] == 10:
            d['cell_type'] = "SCell 0"
            b.append(d)
        else:
            d['cell_type'] = "SCell 1"
            b.append(d)
    first = pd.DataFrame(b)
    # print(first)
    c = []
    for i, d in first.iterrows():
        if d['band'] == 70001:
            d['band'] = "LTE FDD 2100 band 1"
            c.append(d)
        elif d['band'] == 70002:
            d['band'] = "LTE FDD 1900 band 2"
            c.append(d)
        elif d['band'] == 70003:
            d['band'] = "LTE FDD 1800 band 3"
            c.append(d)
        elif d['band'] == 70004:
            d['band'] = "LTE FDD 2100 band 4"
            c.append(d)
        elif d['band'] == 70005:
            d['band'] = "LTE FDD 850 band 5"
            c.append(d)
        elif d['band'] == 70006:
            d['band'] = "LTE FDD 850 band 6"
            c.append(d)
        elif d['band'] == 70007:
            d['band'] = "LTE FDD 2600 band 7"
            c.append(d)
        elif d['band'] == 70008:
            d['band'] = "LTE FDD 900 band 8"
            c.append(d)
        elif d['band'] == 70009:
            d['band'] = "LTE FDD 1800 band 9"
            c.append(d)
        elif d['band'] == 70010:
            d['band'] = "LTE FDD 2100 band 10"
            c.append(d)
        elif d['band'] == 70011:
            d['band'] = "LTE FDD 1400 band 11"
            c.append(d)
        elif d['band'] == 70012:
            d['band'] = "LTE FDD 700 band 12"
            c.append(d)
        elif d['band'] == 70013:
            d['band'] = "LTE FDD 700 band 13"
            c.append(d)
        elif d['band'] == 70014:
            d['band'] = "LTE FDD 700 band 14"
            c.append(d)
        elif d['band'] == 70017:
            d['band'] = "LTE FDD 700 band 17"
            c.append(d)
        elif d['band'] == 70018:
            d['band'] = "LTE FDD 850 band 18"
            c.append(d)
        elif d['band'] == 70019:
            d['band'] = "LTE FDD 850 band 19"
            c.append(d)
        elif d['band'] == 70020:
            d['band'] = "LTE FDD 800 band 20"
            c.append(d)
        elif d['band'] == 70021:
            d['band'] = "LTE FDD 1500 band 21"
            c.append(d)
        elif d['band'] == 70022:
            d['band'] = "LTE FDD 3500 band 22"
            c.append(d)
        elif d['band'] == 70023:
            d['band'] = "LTE FDD 2200 band 23"
            c.append(d)
        elif d['band'] == 70024:
            d['band'] = "LTE FDD 1500 band 24"
            c.append(d)
        elif d['band'] == 70025:
            d['band'] = "LTE FDD 1900 band 25"
            c.append(d)
        elif d['band'] == 70026:
            d['band'] = "LTE FDD 850 band 26"
            c.append(d)
        elif d['band'] == 70027:
            d['band'] = "LTE FDD 800 band 27"
            c.append(d)
        elif d['band'] == 70028:
            d['band'] = "LTE FDD 700 band 28"
            c.append(d)
        elif d['band'] == 70029:
            d['band'] = "LTE FDD 700 band 29"
            c.append(d)
        elif d['band'] == 70030:
            d['band'] = "LTE FDD 2350 band 30"
            c.append(d)
        elif d['band'] == 70031:
            d['band'] = "LTE FDD 450 band 31"
            c.append(d)
        elif d['band'] == 70032:
            d['band'] = "LTE FDD 1500 L-band 32"
            c.append(d)
        elif d['band'] == 70064:
            d['band'] = "LTE FDD 390-470 band 64"
            c.append(d)
        elif d['band'] == 70065:
            d['band'] = "LTE FDD 2100 band 65"
            c.append(d)
        elif d['band'] == 70066:
            d['band'] = "LTE FDD AWS-3 2100 band 66"
            c.append(d)
        elif d['band'] == 70067:
            d['band'] = "LTE FDD 700 EU band 67"
            c.append(d)
        else:
            d['band'] = "LTE FDD FDD"
            c.append(d)
    df1 = pd.DataFrame(c)
    a = [df1, df2]
    frame = pd.concat(a, axis=0, sort=False)
    frame = frame[['scr_file', 'column','time','tech','cell_type','band','ch','pci','rssi','rsrp','rsrq','timing','pathloss','lon','lat']]
    frame.sort_values("time", axis=0, ascending=True, inplace=True, na_position='last')
    df1 = pd.DataFrame(frame).reset_index(drop=True)

    inx1 = 0
    inx2 = 1
    list_index_drop_column_a = []

    batas = len(df1) - 1
    while inx2 <= batas:
        if df1['column'][inx1] == "CELLMEAS" and df1['column'][inx2] == "CELLMEAS":
            nilai_ch = (df1['ch'][inx1] + df1['ch'][inx2]) / 2
            nilai_pci = (df1['pci'][inx1] + df1['pci'][inx2]) / 2
            nilai_rssi = (df1['rssi'][inx1] + df1['rssi'][inx2]) / 2
            nilai_rsrp = (df1['rsrp'][inx1] + df1['rsrp'][inx2]) / 2
            nilai_rsrq = (df1['rsrq'][inx1] + df1['rsrq'][inx2]) / 2
            nilai_pathloss = (df1['pathloss'][inx1] + df1['pathloss'][inx2]) / 2
            df1.loc[inx1, 'ch'] = nilai_ch
            df1.loc[inx1, 'pci'] = nilai_pci
            df1.loc[inx1, 'rssi'] = nilai_rssi
            df1.loc[inx1, 'rsrp'] = nilai_rsrp
            df1.loc[inx1, 'rsrq'] = nilai_rsrq
            df1.loc[inx1, 'pathloss'] = nilai_pathloss
            list_index_drop_column_a.append(inx2)
        elif df1['column'][inx1] == "GPS" and df1['column'][inx2] == "GPS":
            list_index_drop_column_a.append(inx2)

        inx2 = inx2 + 1
        inx1 = inx1 + 1

    df1.drop(df1.index[list_index_drop_column_a], inplace=True)
    # mean_data = df1[['column','time','tech','cell_type','band','ch','pci','rssi','rsrp','rsrq','timing','pathloss','lon','lat']].mean()
    # df1 = df1.replace(np.nan, mean_data)
    df1 = df1.reset_index(drop=True)

    aww = pd.DataFrame(df1)

    in1 = 0
    in2 = 1
    list_aww = []
    list_index_drop_column_c = []
    batas2 = len(aww) - 1
    while in2 <= batas2:
        if aww['column'][in1] == "GPS" and aww['column'][in2] == "CELLMEAS":
            aww.loc[in2, 'lon'] = aww['lon'][in1]
            aww.loc[in2, 'lat'] = aww['lat'][in1]
            # dict_aww = {
            #     "column": aww['column'][in2],
            #     "time": aww['time'][in2],
            #     "tech": aww['tech'][in2],
            #     "cell_type": aww['cell_type'][in2],
            #     "band": aww['band'][in2],
            #     "ch": aww['ch'][in2],
            #     "pci": aww['pci'][in2],
            #     "rssi": aww['rssi'][in2],
            #     "rsrp": aww['rsrp'][in2],
            #     "rsrq": aww['rsrq'][in2],
            #     "timing": aww['timing'][in2],
            #     "pathloss": aww['pathloss'][in2],
            #     "lon": aww['lon'][in1],
            #     "lat": aww['lat'][in1]
            # }
            # list_aww.append(dict_aww)
            list_index_drop_column_c.append(in1)
        else:
            list_index_drop_column_c.append(in2)
        in2 = in2 + 1
        in1 = in1 + 1

    aww.drop(aww.index[list_index_drop_column_c], inplace=True)
    aww = aww.reset_index(drop=True)
    arrgh = pd.DataFrame(aww)

    arrgh['time'] = df3['month'].iloc[0] + " " + arrgh['time']
    # print(arrgh)
    # print(cellmeas)
    # arrgh.to_csv(, sep=',', encoding='iso-8859-1')
    return arrgh


def parsing_cellmeas_3g(cellmeas, gps, month):
    df1 = pd.read_csv(cellmeas, low_memory=False, encoding="iso-8859-1")
    df2 = pd.read_csv(gps, low_memory=False, encoding="iso-8859-1")
    df3 = pd.read_csv(month, low_memory=False, encoding="iso-8859-1")
    df3['month'] = df3['month'].str.replace('.', '/')

    b = []
    for i, d in df1.iterrows():
        if d['cell_type'] == 0:
            d['cell_type'] = "Active"
            b.append(d)
        elif d['cell_type'] == 1:
            d['cell_type'] = "Monitored"
            b.append(d)
        elif d['cell_type'] == 2:
            d['cell_type'] = "Detected"
            b.append(d)
        else:
            d['cell_type'] = "Undetected"
            b.append(d)
    first = pd.DataFrame(b)
    c = []
    for i, d in first.iterrows():
        if d['band'] == 50001:
            d['band'] = "UMTS FDD 2100 band 1"
            c.append(d)
        elif d['band'] == 50002:
            d['band'] = "UMTS FDD 1900 band 2"
            c.append(d)
        elif d['band'] == 50003:
            d['band'] = "UMTS FDD 1800 band 3"
            c.append(d)
        elif d['band'] == 50004:
            d['band'] = "UMTS FDD 2100 band 4"
            c.append(d)
        elif d['band'] == 50005:
            d['band'] = "UMTS FDD 850 band 5"
            c.append(d)
        elif d['band'] == 50006:
            d['band'] = "UMTS FDD 850 band 6"
            c.append(d)
        elif d['band'] == 50007:
            d['band'] = "UMTS FDD 2600 band 7"
            c.append(d)
        elif d['band'] == 50008:
            d['band'] = "UMTS FDD 900 band 8"
            c.append(d)
        elif d['band'] == 50009:
            d['band'] = "UMTS FDD 1800 band 9"
            c.append(d)
        elif d['band'] == 50010:
            d['band'] = "UMTS FDD 2100 band 10"
            c.append(d)
        elif d['band'] == 50011:
            d['band'] = "UMTS FDD 1400 band 11"
            c.append(d)
        elif d['band'] == 50012:
            d['band'] = "UMTS FDD 500 band 12"
            c.append(d)
        elif d['band'] == 50013:
            d['band'] = "UMTS FDD 500 band 13"
            c.append(d)
        elif d['band'] == 50014:
            d['band'] = "UMTS FDD 500 band 14"
            c.append(d)
        elif d['band'] == 50019:
            d['band'] = "UMTS FDD 850 band 19"
            c.append(d)
        elif d['band'] == 50020:
            d['band'] = "UMTS FDD 800 band 20"
            c.append(d)
        elif d['band'] == 50021:
            d['band'] = "UMTS FDD 1500 band 21"
            c.append(d)
        elif d['band'] == 50022:
            d['band'] = "UMTS FDD 3500 band 22"
            c.append(d)
        elif d['band'] == 50023:
            d['band'] = "UMTS FDD 2200 band 23"
            c.append(d)
        elif d['band'] == 50024:
            d['band'] = "UMTS FDD 1500 band 24"
            c.append(d)
        elif d['band'] == 50025:
            d['band'] = "UMTS FDD 1900 band 25"
            c.append(d)
        elif d['band'] == 50026:
            d['band'] = "UMTS FDD 850 band 26"
            c.append(d)
        else:
            d['band'] = "UMTS FDD FDD"
            c.append(d)

    df1 = pd.DataFrame(c)
    a = [df1, df2]
    frame = pd.concat(a, axis=0, ignore_index=True, sort=False)
    frame.sort_values("time", axis=0, ascending=True, inplace=True, na_position='last')
    frame = frame[['column','time','tech','cell_type','band','ch','sc','ec','sttd','rscp','lon','lat']]
    df = frame.reset_index(drop=True)

    inx1 = 0
    inx2 = 1
    list_index_drop_column_a = []

    batas = len(df) - 1
    while inx2 <= batas:
        if df['column'][inx1] == "CELLMEAS" and df['column'][inx2] == "CELLMEAS":
            nilai_ch = (df['ch'][inx1] + df['ch'][inx2]) / 2
            nilai_sc = (df['sc'][inx1] + df['sc'][inx2]) / 2
            nilai_ec = (df['ec'][inx1] + df['ec'][inx2]) / 2
            nilai_rscp = (df['rscp'][inx1] + df['rscp'][inx2]) / 2
            df.loc[inx1, 'ch'] = nilai_ch
            df.loc[inx1, 'sc'] = nilai_sc
            df.loc[inx1, 'ec'] = nilai_ec
            df.loc[inx1, 'rscp'] = nilai_rscp
            list_index_drop_column_a.append(inx2)
        elif df['column'][inx1] == "GPS" and df['column'][inx2] == "GPS":
            list_index_drop_column_a.append(inx2)

        inx2 = inx2 + 1
        inx1 = inx1 + 1
    df.drop(df.index[list_index_drop_column_a], inplace=True)
    df = df.reset_index(drop=True)
    aww = pd.DataFrame(df)
    aa = [aww,df3]
    frame = pd.concat(a, axis=0, sort=False)

    asw1 = 0
    asw2 = 1
    list_aww = []
    list_index_drop_column_c = []
    batas2 = len(aww) - 1
    while asw2 <= batas2:
        if aww['column'][asw1] == "GPS" and aww['column'][asw2] == "CELLMEAS":
            aww.loc[asw2, 'lon'] = aww['lon'][asw1]
            aww.loc[asw2, 'lat'] = aww['lat'][asw1]
            list_index_drop_column_c.append(asw1)
        else:
            list_index_drop_column_c.append(asw2)
        asw2 = asw2 + 1
        asw1 = asw1 + 1

    aww.drop(df.index[list_index_drop_column_c], inplace=True)
    df = aww.reset_index(drop=True)
    arrgh = pd.DataFrame(df)

    arrgh['time'] = df3['month'].iloc[0] + " " + arrgh['time']

    return arrgh


def parsing_cellmeas_2g(cellmeas,gps,month):
    df1 = pd.read_csv(cellmeas, low_memory=False, encoding="iso-8859-1")
    df2 = pd.read_csv(gps, low_memory=False, encoding="iso-8859-1")
    df3 = pd.read_csv(month, low_memory=False, encoding="iso-8859-1")
    df3['month'] = df3['month'].str.replace('.', '/')

    b = []
    for i, d in df1.iterrows():
        if d['cell_type'] == 0:
            d['cell_type'] = "Neighbor"
            b.append(d)
        else:
            d['cell_type'] = "Serving"
            b.append(d)
    first = pd.DataFrame(b)
    c = []
    for i, d in first.iterrows():
        if d['band'] == 10850:
            d['band'] = "GSM 850"
            c.append(d)
        elif d['band'] == 10900:
            d['band'] = "GSM 900"
            c.append(d)
        elif d['band'] == 11800:
            d['band'] = "GSM 1800"
            c.append(d)
        elif d['band'] == 11900:
            d['band'] = "GSM 1900"
            c.append(d)
        else:
            d['band'] = "GSM"
            c.append(d)

    df1 = pd.DataFrame(c)
    a = [df1, df2]
    frame = pd.concat(a, axis=0, ignore_index=True, sort=False)
    frame.sort_values("time", axis=0, ascending=True, inplace=True, na_position='last')
    frame = frame[['column','time','tech','cells','cell_type','band','arfcn','bsic','rxlev_full','rxlev_sub','c1','c2','c31','c32','hcs_priority','hcs_thr','cell_id','lac','rac','srxlev','lon','lat']]
    df = frame.reset_index(drop=True)

    inx1 = 0
    inx2 = 1
    list_index_drop_column_a = []

    batas = len(df) - 1
    while inx2 <= batas:
        if df['column'][inx1] == "CELLMEAS" and df['column'][inx2] == "CELLMEAS":
            nilai_arfcn = (df['arfcn'][inx1] + df['arfcn'][inx2]) / 2
            nilai_bsic = df['bsic'][inx1]
            nilai_rxlev_full = (df['rxlev_full'][inx1] + df['rxlev_full'][inx2]) / 2
            nilai_rxlev_sub = (df['rxlev_sub'][inx1] + df['rxlev_sub'][inx2]) / 2
            nilai_c1 = (df['c1'][inx1] + df['c1'][inx2]) / 2
            nilai_c2 = (df['c2'][inx1] + df['c2'][inx2]) / 2
            nilai_c31 = (df['c31'][inx1] + df['c31'][inx2]) / 2
            nilai_c32 = (df['c32'][inx1] + df['c32'][inx2]) / 2
            nilai_hcs_priority = (df['hcs_priority'][inx1] + df['hcs_priority'][inx2]) / 2
            nilai_hcs_thr = (df['hcs_thr'][inx1] + df['hcs_thr'][inx2]) / 2
            nilai_cell_id = df['cell_id'][inx1]
            nilai_lac = (df['lac'][inx1] + df['lac'][inx2]) / 2
            nilai_rac = (df['rac'][inx1] + df['rac'][inx2]) / 2
            nilai_srxlev = (df['srxlev'][inx1] + df['srxlev'][inx2]) / 2
            df.loc[inx1, 'arfcn'] = nilai_arfcn
            df.loc[inx1, 'bsic'] = nilai_bsic
            df.loc[inx1, 'rxlev_full'] = nilai_rxlev_full
            df.loc[inx1, 'rxlev_sub'] = nilai_rxlev_sub
            df.loc[inx1, 'c1'] = nilai_c1
            df.loc[inx1, 'c2'] = nilai_c2
            df.loc[inx1, 'c31'] = nilai_c31
            df.loc[inx1, 'c32'] = nilai_c32
            df.loc[inx1, 'hcs_priority'] = nilai_hcs_priority
            df.loc[inx1, 'hcs_thr'] = nilai_hcs_thr
            df.loc[inx1, 'cell_id'] = nilai_cell_id
            df.loc[inx1, 'lac'] = nilai_lac
            df.loc[inx1, 'rac'] = nilai_rac
            df.loc[inx1, 'srxlev'] = nilai_srxlev
            list_index_drop_column_a.append(inx2)
        elif df['column'][inx1] == "GPS" and df['column'][inx2] == "GPS":
            list_index_drop_column_a.append(inx2)

        inx2 = inx2 + 1
        inx1 = inx1 + 1
    df.drop(df.index[list_index_drop_column_a], inplace=True)
    df = df.reset_index(drop=True)
    aww = pd.DataFrame(df)

    asw1 = 0
    asw2 = 1
    list_aww = []
    list_index_drop_column_c = []
    batas2 = len(aww) - 1
    while asw2 <= batas2:
        if aww['column'][asw1] == "GPS" and aww['column'][asw2] == "CELLMEAS":
            aww.loc[asw2, 'lon'] = aww['lon'][asw1]
            aww.loc[asw2, 'lat'] = aww['lat'][asw1]
            list_index_drop_column_c.append(asw1)
        else:
            list_index_drop_column_c.append(asw2)
        asw2 = asw2 + 1
        asw1 = asw1 + 1

    aww.drop(df.index[list_index_drop_column_c], inplace=True)
    df = aww.reset_index(drop=True)
    arrgh = pd.DataFrame(df)

    arrgh['time'] = df3['month'].iloc[0] + " " + arrgh['time']

    return arrgh

if __name__ == "__main__":
    pd.set_option('max_columns', 10000)
    pd.set_option('display.width', 10000)
    pd.set_option('display.max_rows', None)
    # pd.option_context('display.max_rows',  1000)
    path4g = '/home/immobi/Documents/logfile/OneDrive-2020-01-28/4G/'
    path3g = '/home/immobi/Documents/logfile/OneDrive-2020-01-28/3G/'
    path2g = '/home/immobi/Documents/logfile/OneDrive-2020-01-28/2G/'
    gps,month,cellmeas2g,cellmeas3g,cellmeas4g = load_nmf(path4g)
    df = parsing_cellmeas_4g(cellmeas4g,gps,month)
    df.reset_index()
    pathexport = path4g + df['scr_file'][0] + "_result_4g.csv"
    df.to_csv(pathexport,sep=',',encoding='iso-8859-1',header=True)
    # print(pathexport)
    print(df)