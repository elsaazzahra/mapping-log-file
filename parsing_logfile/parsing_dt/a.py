import pandas as pd
def arr():
    arr = [1,2,3,4,5,6,7,7,9]
    dic = [{"nama" : "taufik","rombel" : "rpl"},{"nama" : "dobleh", "rombel" : "mmd"},{"nama" : "dobleh", "rombel" : "TKJ"},{"nama" : "taufik", "rombel" : "apk"}]
    df= pd.DataFrame(dic)
    inx1 = 0
    inx2 = 1
    a = []
    for i,d in df.iterrows():
        if df['nama'][inx1] == "dobleh" and df['nama'][inx2] == "dobleh":
            dec = {
                "nama" : df['nama'][inx1],
                "rombel": df['rombel'][inx1]
            }
            a.append(dec)

        elif df['nama'][inx1] == "dobleh" and df['nama'][inx2] == "dobleh":
            dec = {
                "nama": df['nama'][inx2],
                "rombel": df['rombel'][inx2]
            }
            a.append(dec)
        else:
            dec1 = {
                "nama": df['nama'][inx1],
                "rombel": df['rombel'][inx1]
            }

            a.append(dec1)

        inx1 = inx1 + 1
        inx2 = inx2 + 1

    print(a)

def arr2():
    dic = [{"nama" : "taufik","rombel" : "rpl","nilai": 7},{"nama" : "dobleh", "rombel" : "mmd", "nilai":8} ,{"nama" : "dobleh", "rombel" : "TKJ","nilai" : 5},{"nama" : "taufik", "rombel" : "apk", "nilai" : 2},{"nama" : "taufik", "rombel" : "apk", "nilai" : 5},{"nama" : "taufik", "rombel" : "apk", "nilai" : 5},{"nama" : "dobleh", "rombel" : "mmd", "nilai":8},{"nama" : "dobleh", "rombel" : "mmd", "nilai":8}]
    df= pd.DataFrame(dic)
    inx1 = 0
    inx2 = 1
    list_index_drop_column_a = []

    list_index_drop_column_b = []
    print(df)
    batas = len(df) -1
    while inx2 <= batas:
        if df['nama'][inx1] == "dobleh" and df['nama'][inx2] == "dobleh":
            nilai_res = df['nilai'][inx1] + df['nilai'][inx2]
            df.loc[inx1, 'nilai'] = nilai_res
            list_index_drop_column_a.append(inx2)
        elif df['nama'][inx1] == "taufik" and df['nama'][inx2] == "taufik":
            list_index_drop_column_a.append(inx2)

        inx2 = inx2 + 1
        inx1 = inx1 + 1
    df.drop(df.index[list_index_drop_column_a],inplace=True)
    print(list_index_drop_column_a)
    print(df)

if __name__ == "__main__":
    arr2()